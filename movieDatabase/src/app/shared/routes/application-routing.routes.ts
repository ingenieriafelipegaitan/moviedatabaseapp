import { Routes } from "@angular/router";

export const app_ROUTES: Routes = [
  // main-module
  {
    path: "main",
    loadChildren: () =>
      import("../../main/main.module").then((m) => m.MainModule),
  },
  // actor-module
  {
    path: "actor",
    loadChildren: () =>
      import("../../actor/actor.module").then((m) => m.ActorModule),
  },
];
