import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./main/pages/home/home.component";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
  },
  {
    path: "actor",
    loadChildren: () =>
      import("../app/actor/actor.module").then((m) => m.ActorModule),
  },
  {
    path: "movie",
    loadChildren: () =>
      import("../app/movie/movie.module").then((m) => m.MovieModule),
  },
  {
    path: "**",
    pathMatch: "full",
    redirectTo: "",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
