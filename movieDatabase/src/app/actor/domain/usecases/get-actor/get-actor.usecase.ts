import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { GetActorGateway } from "../../models/get-actor/get-actor.gateway";

@Injectable()
export class GetActorUseCase {
  constructor(private actorGateway: GetActorGateway) {}

  callService(params: any): Observable<any> {
    return this.actorGateway.getActor(params);
  }
}
