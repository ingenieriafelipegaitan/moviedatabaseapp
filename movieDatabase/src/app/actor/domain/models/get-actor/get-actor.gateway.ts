import { Observable } from "rxjs";

export abstract class GetActorGateway {
  abstract getActor(params: any): Observable<any>;
}
