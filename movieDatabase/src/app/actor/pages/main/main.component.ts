import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { IActor } from "../../domain/models/actor.interface";
import { GetActorUseCase } from "../../domain/usecases/get-actor/get-actor.usecase";
import Swal from "sweetalert2";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"],
})
export class MainComponent implements OnInit {
  params: any;
  response: any;
  form = new FormGroup({
    actor: new FormControl(""),
  });
  displayedColumns: string[] = ["id", "name", "more"];
  dataSource: IActor;
  constructor(
    private router: Router,
    private getActorUseCase: GetActorUseCase
  ) {}

  ngOnInit() {}

  getActors(): void {
    this.params = this.paramsCheckValidBuild();
    this.response = this.getActorUseCase.callService(this.params);
    this.response.subscribe((data) => {
      if (data.length > 0) {
        this.dataSource = data;
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "We don't have results for this... :(!",
        });
      }
    });
  }

  getMore(movies: any): void {
    if (movies) {
      const moviesList = [...movies];
      var list = moviesList.map(m => m.title);
    }
    Swal.fire({
      title: "<strong><u>Actor Films</u></strong>",
      html:
        "<b>Movies </b>" + list,
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText: "Great!",
      confirmButtonAriaLabel: "Thumbs up, great!",
    });
  }


  private paramsCheckValidBuild() {
    return {
      query: this.form.get("actor").value,
      language: "en-US",
      page: "1",
    };
  }
}
