import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ActorRoutingModule } from "./actor-routing.module";
import { MainComponent } from "./pages/main/main.component";
import { MaterialModule } from "../shared/material/material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { GetActorUseCase } from "./domain/usecases/get-actor/get-actor.usecase";
import { GetActorGateway } from "./domain/models/get-actor/get-actor.gateway";
import { GetActorService } from "./infrastructure/ActorServices/get-actor.service";
import { HttpClientModule } from "@angular/common/http";
import { DetailComponent } from './pages/detail/detail.component';

@NgModule({
  declarations: [MainComponent, DetailComponent],
  imports: [
    CommonModule,
    ActorRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    GetActorUseCase,
    {
      provide: GetActorGateway,
      useClass: GetActorService,
    },
  ],
})
export class ActorModule {}
