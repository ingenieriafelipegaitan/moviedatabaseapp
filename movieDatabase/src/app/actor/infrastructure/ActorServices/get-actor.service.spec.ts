import { TestBed } from '@angular/core/testing';

import { GetActorService } from './get-actor.service';

describe('GetActorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetActorService = TestBed.get(GetActorService);
    expect(service).toBeTruthy();
  });
});
