import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MainRoutingModule } from "./main-routing.module";
import { HomeComponent } from "./pages/home/home.component";
import { MaterialModule } from "../shared/material/material.module";
import { FooterComponent } from "./pages/footer/footer.component";
import { NavbarComponent } from "./pages/navbar/navbar.component";

@NgModule({
  declarations: [HomeComponent, NavbarComponent, FooterComponent],
  imports: [CommonModule, MainRoutingModule, MaterialModule],
  exports: [NavbarComponent, FooterComponent],
})
export class MainModule {}
