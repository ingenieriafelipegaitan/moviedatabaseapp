import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ListMovieComponent } from "./pages/list-movie/list-movie.component";
import { MaterialModule } from "../shared/material/material.module";
import { MovieRoutingModule } from "./movie-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GetMovieGateway } from "./domain/models/get-movie.gateway";
import { GetMovieUseCase } from "./domain/usecases/get-actor.usecase";
import { GetMovieService } from "./infrastructure/get-movie.service";

@NgModule({
  declarations: [ListMovieComponent],
  imports: [
    CommonModule,
    MovieRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    GetMovieUseCase,
    {
      provide: GetMovieGateway,
      useClass: GetMovieService,
    },
  ],
})
export class MovieModule {}
