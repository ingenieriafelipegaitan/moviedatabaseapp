import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { GetMovieGateway } from "../models/get-movie.gateway";

@Injectable()
export class GetMovieUseCase {
  constructor(private movieGaateway: GetMovieGateway) {}

  callService(params: any): Observable<any> {
    return this.movieGaateway.getMovie(params);
  }
}
