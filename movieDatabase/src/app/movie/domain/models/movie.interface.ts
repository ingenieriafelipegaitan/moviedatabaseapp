export interface IMovie {
  id: string;
  original_title: string;
  release_date: string;
}
