import { Observable } from "rxjs";

export abstract class GetMovieGateway {
  abstract getMovie(params: any): Observable<any>;

}
