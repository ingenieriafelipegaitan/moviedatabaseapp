import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import {
  API,
  API_SEARCH,
} from "src/app/shared/constants/microservices.constants";
import { GetMovieGateway } from "../domain/models/get-movie.gateway";

@Injectable({
  providedIn: "root",
})
export class GetMovieService extends GetMovieGateway {
  api = API;
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
    }),
  };
  getMovie(params: any): Observable<any> {
    let parameters = new HttpParams()
      .set("api_key", this.api)
      .set("language", params.language)
      .set("query", params.query)
      .set("page", params.page);

    return this.httpClient
      .get<any>(`${API_SEARCH}/movie`, {
        headers: this.httpOptions.headers,
        params: parameters,
      })
      .pipe(
        map((response) => {
          const values = response.results;
          return values;
        })
      )
      .pipe(
        catchError((err) => {
          console.error("Server Says: ", err);
          return err;
        })
      );
  }

  constructor(private httpClient: HttpClient) {
    super();
  }
}
