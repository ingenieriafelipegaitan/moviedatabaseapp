import { AfterViewInit, Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { IMovie } from "../../domain/models/movie.interface";
import { GetMovieUseCase } from "../../domain/usecases/get-actor.usecase";
import Swal from "sweetalert2";

@Component({
  selector: "app-list-movie",
  templateUrl: "./list-movie.component.html",
  styleUrls: ["./list-movie.component.scss"],
})
export class ListMovieComponent implements AfterViewInit {
  params: any;
  response: any;
  form = new FormGroup({
    movie: new FormControl(""),
  });
  original_title: string;
  displayedColumns: string[] = ["id", "original_title", "release_date", "more"];
  dataSource: IMovie;

  constructor(
    private router: Router,
    private getMovieUseCase: GetMovieUseCase
  ) {}

  ngAfterViewInit() {}

  getMovies(): void {
    this.params = this.paramsCheckValidBuild();
    this.response = this.getMovieUseCase.callService(this.params);
    this.response.subscribe((data) => {
      if (data.length > 0) {
        this.dataSource = data;
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "We don't have results for this... :(!",
        });
      }
    });
  }

  getMore(poster: any): void {
    if (poster) {
      var poster_path = "https://api.themoviedb.org/" + poster;
    }
    Swal.fire({
      title: "<strong><u>Actor Films</u></strong>",
      html: "<img src=" + poster_path + ">",
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      confirmButtonText: "Great!",
      confirmButtonAriaLabel: "Thumbs up, great!",
    });
  }

  private paramsCheckValidBuild() {
    return {
      query: this.form.get("movie").value,
      language: "en-US",
      page: "1",
    };
  }
}
