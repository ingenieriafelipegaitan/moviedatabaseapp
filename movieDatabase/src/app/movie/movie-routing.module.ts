import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ListMovieComponent } from "./pages/list-movie/list-movie.component";

const routes: Routes = [
  {
    path: "list-movies",
    component: ListMovieComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MovieRoutingModule {}
